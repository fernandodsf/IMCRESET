<%-- 
    Document   : index
    Created on : 06/06/2017, 01:37:05
    Author     : Fernando
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
  <title>Cálculo do IMC</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection" />
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection" />
</head>

<body>
  <div class="parallax"><img src="images/header-bg.jpg"></div>
  <nav class="blue accent-2" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">IMC</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="index.html">Inicio</a></li>
      </ul>

      <ul id="nav-mobile" class="side-nav">
        <li><a href="index.html">Inicio</a></li>
      </ul>
      <a href="index.html" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
  </nav>
  

  


  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h1 class="header center orange-text">Calcule o seu Índice de Massa Corporal</h1>
      <div class="row center">
        <h5 class="header col s12 light">Saiba se você está em forma para o Verão!</h5>

        <form method="get">
        <div class="row center">
          <div class="input-field col s4 offset-s4 valign">
            <input name ="peso" type="text" placeholder="Altura em cm" data-length="3"> <br/>
            <input name ="altura" type="text" placeholder="Peso em kg" data-length="3"> <br/>
            
            </form>
            <button class="btn waves-effect waves-light" type="submit" value="calcular">Calcular
    <i class="material-icons right">send</i>
  </button>
 
            
        <div class="card-panel  blue lighten-5"><span class="blue-text text-darken-2"><b>${resultado}</span></div>

          </div>
        </div>


        
      </div>


    </div>
  </div>




  <footer class="page-footer blue accent-2">
    <div class="container">
      <div class="row">
        <div class="col s7 offset-s1 valign">
          <h5 class="white-text">Sobre o IMC</h5>
          <p class="grey-text text-lighten-4">O IMC é um índice aceito pela Organização Mundial de Saúde (OMS), que estabelece as faixas de índice que indicam
            se uma pessoa está ou não dentro do peso considerado ideal. Muitos nutricionistas e educadores físicos gostam
            de calcular IMC e o usam como ferramenta primária para identificar problemas de peso de uma pessoa, por causa
            da facilidade de cálculo. O IMC assume que existe uma faixa de peso ideal para sua altura. A partir de uma conta
            que usa seu peso e sua altura, encontra-se um valor (índice).
          </p>


        </div>

      </div>
    </div>
    
    <div class="footer-copyright">
      <div class="container">

        Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>

    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

</body>

</html>