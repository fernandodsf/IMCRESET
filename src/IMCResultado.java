import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class IMCResultado {
	
	private Float imc;
	private String descricao;
	
	public Float getimc() {
		return imc;
	}
	public void setimc(Float imc) {
		this.imc = imc;
	}
	public String getdescricao() {
		return descricao;
	}
	public void setdescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
